# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-05)

### Edited

* Build the feature hierarchy.

### Fixed

* Duplicated gene feature: LALA0_S01e17084g.
* LTR LALA0_S01e02014t on reverse strand.
* Mobile element with multiple locus_tag: LALA0_S01e11078t.
* Mobile element with multiple locus_tag: LALA0_S01e17084t.
* Mobile element with multiple locus_tag: LALA0_S01e18514t.
* LTR LALA0_S02e01794t on reverse strand.
* Mobile element with multiple locus_tag: LALA0_S02e04148t.
* Mobile element with multiple locus_tag: LALA0_S02e05358t.
* Mobile element with multiple locus_tag: LALA0_S02e05380t.
* Mobile element with multiple locus_tag: LALA0_S02e11188t.
* Mobile element with multiple locus_tag: LALA0_S03e09076t.
* Mobile element with multiple locus_tag: LALA0_S03e09230t.
* Mobile element with multiple locus_tag: LALA0_S03e10308t.
* Bad mobile_element coordinates: LALA0_S03e09230t.
* Mobile element with multiple locus_tag: LALA0_S05e01926t.
* Mobile element with multiple locus_tag: LALA0_S06e00452t.
* Mobile element with multiple locus_tag: LALA0_S07e03928t.
* Mobile element with multiple locus_tag: LALA0_S07e04258t.
* Mobile element with multiple locus_tag: LALA0_S07e04456t.
* Mobile element with multiple locus_tag: LALA0_S11e01332t.
* LTR LALA0_S12e00408t on reverse strand.
* LTR LALA0_S12e00892t on reverse strand.
* Mobile element with multiple locus_tag: LALA0_S12e00892t.
* Mobile element with multiple locus_tag: LALA0_S13e03136t.
* Mobile element with multiple locus_tag: LALA0_S14e00958t.
* Mobile element with multiple locus_tag: LALA0_S15e00276t.
* LTR LALA0_S15e01728t on reverse strand.
* Mobile element with multiple locus_tag: LALA0_S22e00100t.
* Missing stop codon: LALA0_S02e06282g.
* Missing stop codon: LALA0_S10e02036g.
* Bad Met and stop codon: LALA0_S13e02454g.
* LALA0_S22e00232g interupted by scaffold end. Turned into pseudo.

## v1.0 (2021-05-05)

### Added

* The 7 annotated chromosomes of Lachancea lanzarotensis CBS 12615 (source EBI, [GCA_000938715.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000938715.1)).
