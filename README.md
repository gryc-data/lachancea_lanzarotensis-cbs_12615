## *Lachancea lanzarotensis* CBS 12615

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB7950](https://www.ebi.ac.uk/ena/browser/view/PRJEB7950)
* **Assembly accession**: [GCA_000938715.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000938715.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: LALA0
* **Assembly length**: 11,092,131
* **#Scaffold**: 24
* **Mitochondiral**: No
* **N50 (L50)**: 910,667 (5)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5117
* **Pseudogene count**: 59
* **tRNA count**: 199
* **rRNA count**: 0
* **Mobile element count**: 32
